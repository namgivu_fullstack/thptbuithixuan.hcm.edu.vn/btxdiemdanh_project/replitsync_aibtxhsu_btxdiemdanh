from pathlib import Path
import json


def dangnhap(email):
  """
  quitrinh
  https://www.notion.so/03-dang-nhap-fd5e605c1c884e87b0a033d23042d1a7
  """
  
  #region xacthuc email
  '''co email trong taikhoan_list'''
  # taikhoan_list nap tu tep json
  THIS_DIR = Path(__file__).parent
  json_p   = THIS_DIR/'s2311161400_dangky.data.json'
  json_s   = json_p.read_text()
  taikhoan_list = json.loads(json_s)

  # email ktra
  ok_email = email in taikhoan_list
  #endregion xacthuc email

  if not ok_email:
    return 'loi'
    pass#todo
    
  else:
    '''gui email maxacnhan'''
    #region tao maxacnhan
    def binh_sol(level):
      import random
      mxn=''
      if level == 6:
        mxn = str(random.randint(0,9))*6

      
      elif level == 3:
        # ba so dau tien
        sodau = random.randint(0,9)
        mxn = str(sodau)*3
        
        # ba so sau khac ba so dau tien
        sosau = sodau
        while sosau == sodau:
          sosau = random.randint(0,9)
        mxn += str(sosau)*3

      
      elif level == 2:
        # hai so dau tien
        sodau = random.randint(0,9)
        mxn = str(sodau)*2
        # hai so sau khac hai so dau tien
        sosau = sodau
        while sosau == sodau:
          sosau = random.randint(0,9)
        mxn += str(sosau)*2
        # hai so cuoi khac bon so dau tien
        socuoi = sodau
        while (socuoi == sodau) or (socuoi == sosau):
          socuoi = random.randint(0,9)
        mxn += str(socuoi)*2
      
      else:
        mxn = str(random.randint(100000,999999))
      
      return mxn
      
    def khanh_sol(length=6):
      import random
      sample_string = 'abcdefghijklmnopqrstuvwxyz0123456789'
      sample_level  = '6320'
      
      easylevel=int(random.choice(sample_level))
      
      if easylevel == 6:
        return random.choice(sample_string)*6
      elif easylevel == 3:
        return (random.choice(sample_string)*3)+(random.choice(sample_string)*3)
      elif easylevel == 2:
        return (random.choice(sample_string)*2)+(random.choice(sample_string)*2)+(random.choice(sample_string)*2)
      else:
        return [random.choice(sample_string) for _ in range(length)]

    def khoa_sol(length=6):
      import secrets
      import random as r
      characters = "abcdefghijklmnopqrstuvwxyz0123456789"
      level = r.choice([0,2,3,6])
    
      if level == 2:
        character1= secrets.choice(characters)
        character2= secrets.choice(characters)
        character3= secrets.choice(characters)
        word = character1*2+character2*2+character3*2
      elif level == 3:
        character_lv3_1= secrets.choice(characters)
        character_lv3_2= secrets.choice(characters)
        word = character_lv3_1*3+character_lv3_2*3
      elif level == 6:
         character_lv6= secrets.choice(characters)
         word = character_lv6*6
       elif level == 0:
         word = "".join(secrets.choice(characters) for _ in range(length))
      return word
    
    def toan_sol(length):
      import random
      capdo = random.randint(1, 3)
      if (capdo == 1):
        characters = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        mxn = "".join(random.choice(characters) for _ in range(length))
        mxn *= 6
        return mxn
      elif (capdo == 2):
        characters = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        mxn = "".join(random.choice(characters) for _ in range(length))
        mxn = mxn[0] * 3 + mxn[1] * 3
        return mxn
      else:
        characters = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        mxn = "".join(random.choice(characters) for _ in range(length))
        mxn = mxn[0] * 2 + mxn[1] * 2 + mxn[2] * 2
        return mxn
    print(toan_sol(6))
    #endregion tao maxacnhan
    
    pass#todo


if __name__=='__main__':
  r = dangnhap(email='e.mail@c.om')    ; print(r)
  r = dangnhap(email='e.mail@c.omEEE') ; print(r)
  